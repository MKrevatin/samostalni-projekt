<?php 
session_start();
if(!isset($_SESSION['login']))
{  
	header("Location: index.html");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BOLNICA</title>
<link rel="stylesheet" href="css1.css" type="text/css" />
</head>

<body>
<div id="container">
    <div id="header">
        <h1><a href="profil.php">Bolnica</a></h1>
    </div>
    <div id="menu">
    <div style="width:50%; margin:0 auto;">
    <?php
	
	
       echo '<p>Boja članaka:</p>';
       echo '<form id="form_boja" method="post" action="" name="boja">
            <input type="radio" name="boja" value="#05f600"> <label for="boja">Zelena</label><br />
            <input type="radio" name="boja" value="#26a8f6"> <label for="boja">Plava</label><br />
            <input type="radio" name="boja" value="#f64747"> <label for="boja">Crvena</label><br />
            <input class="submit" type="submit" name="potvrda" value="Oboji"><br />
        </form>';
        
        if(isset($_POST['boja'])){
            $boja = $_POST['boja'];
            setcookie("boja",$boja,time()+60*60*24*30); // trajanje Cookie-a 
            header("Location: profil.php");
        }
        if(!isset($_COOKIE['boja'])){
            $boja = "#05f600";
        }
        else{
            $boja = $_COOKIE['boja'];
        }
    ?>
    <p><a href="korisnik.php">Izmjena podataka</a></p>
    <p><a href="odjava.php">Odjava</a></p>    
    </div>
	</div>
    <div id="content">
    <div style="width:80%; margin:0 auto;">
		<?php
        
        $c = new mysqli("localhost",
                        "root",
                        "123",
                        "dwa2");
        mysqli_set_charset($c, "utf8");				
        $sql = "SELECT * FROM user WHERE email='" . $_SESSION['login'] . "'";
        $r = $c->query($sql);
        $row = $r->fetch_assoc();
        
        echo '<div>';
        echo '<h2>Dobrodošli '. $row['username'] .'!</h2>';     
        echo '<div id="vijest"><h2>Vaše vijesti:</h2><br/>';

        $sql2 = "SELECT * from vijest ORDER BY id_vijest DESC";
        $r2 = $c->query($sql2);
        
        while($row2 = $r2->fetch_assoc()){
            $sql3 = "SELECT * FROM suser WHERE id_suser='". $row2['id_suser'] ."'";
            $r3 = $c->query($sql3);
            $row3 = $r3->fetch_assoc();
            
            $id = $row2['id_vijest'];
            echo '
            <table border="1px"  style="background-color:'. $boja .'; border-radius:10px;" >
                <tr>
                    <td>'. 'Napisao: ' . $row3['ime'].' '. $row3['prezime']. ', '. $row3['tip'] .'</td>
                    <td>'. 'Objavljeno: ' . $row2['datum_vijest'] .'</td>
                </tr>
                <tr>
                    <td colspan="2">'.'Naslov: ' .$row2['naslov'] .' <a href="clanak.php?id_vijest='. $id.'">...više</a></td>
                </tr>
            </table>
            <br />';
        }
        
        $c->close();
        
        ?>
    </div>
	</div>
    
</div></div><div id="footer">
        Marco Krevatin - Dinamičke Web Aplikacije 2
    </div>
</body>
</html>