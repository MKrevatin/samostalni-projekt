<?php 
session_start();
if(!isset($_SESSION['alogin']))
{  
	header("Location: index.html");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administracija - profil</title>
<link href="../css1.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="container">
    <div id="header">
        <h1><a href="profil.php">Bolnica</a></h1>
    </div>
    <div id="menu">
    <div style="width:50%; margin:0 auto;">
	<p><a href="unos.php">Unos novog članka</a></p>
    <p><a href="korisnik.php">Izmjena podataka</a></p>
    <p><a href="odjava.php">Odjava</a></p></div>
<?php

$c = new mysqli("localhost",
				"root",
				"123",
				"dwa2");
mysqli_set_charset($c, "utf8");
				
$sql = "SELECT * FROM suser WHERE email='" . $_SESSION['alogin'] . "'";
$r = $c->query($sql);
$row = $r->fetch_assoc();
?>   
    </div>
    <div id="content">
    <div style="width:80%; margin:0 auto;">
<?php
echo '<h2>Dobrodošli '. $row['ime'] .' '. $row['prezime'] .'!</h2>';

$sql2 = "SELECT * FROM vijest WHERE id_suser='". $row['id_suser'] ."' ORDER BY id_vijest DESC";
$r2 = $c->query($sql2);
echo '<div ><h2>Moje vijesti:</h2>';
while($row2 = $r2->fetch_assoc()){
	$id = $row2['id_vijest'];
	echo '
	<table border="1">
		<tr>
			<td>'. 'Napisao: ' . $row['ime'].' '. $row['prezime']. ', '. $row['tip'] .'</td>
			<td>'. 'Objavljeno: ' . $row2['datum_vijest'] .'</td>
		</tr>
		<tr>
			<td colspan="2">'.'Naslov: ' .$row2['naslov'] .'</td>
		</tr>
		<tr>
			<td colspan="2"><a href="izmjena.php?id_vijest='. $id.'">IZMJENA</a> <a href="brisanje.php?id_vijest='. $id.'">BRISANJE</a></td>
		</tr>
	</table>
	<br />';
}

$c->close();
?>
</div>
</div>
    
</div><div id="footer">
        Marco Krevatin - Dinamičke Web Aplikacije 2
    </div>
</body>
</html>