<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registracija</title>
<link rel="stylesheet" href="css1.css" type="text/css" />
</head>

<body>
<div id="container">
    <div id="header">
        <h1><a href="index.html">Bolnica</a></h1>
    </div>
    <div id="menu"><div style="padding-left:10px; padding-top:10px; margin:0 auto;">
	<form method="post" action="prijava.php">
        <div><label for="email">Email</label> <input class="input" type="text" name="email" placeholder="Email"/></div>
        <div><label for="password">Lozinka</label> <input class="input" type="password" name="password" placeholder="Password" /></div>
        <div><input class="submit" type="submit" value="Log In" /></div>
        <div><a href="registracija.php">Registracija</a></div>
    </form>
	<p><a href="admin/index.html">Administracija</a></p>
	<a href="index.html">Povratak na naslovnicu</a>
    </div>
    </div>
    <div id="content">
        <a href="index.html"></a>
            <?php if(!$_POST){ ?>
            <div style="width:40%; margin:0 auto;">
            <h2>Registracija</h2>
            </div>
            <div style="width:50%; margin:0 auto;">
            
            <table>
            <tr>
            <td> 
            <form id="form_register" name="registracija" method="post" action=""><br />
                <label for="ime">*Ime</label><input name="ime" type="text" placeholder="Ime"><br />
                <label for="prezime">*Prezime</label><input name="prezime" type="text" placeholder="Prezime"><br />
                <label for="username">*Username</label><input name="username" type="text" placeholder="username"><br />
		        <label for="password">*Lozinka</label><input name="password" type="password" placeholder="Password"><br />
                <label for="email">*Email</label><input name="email" type="text" placeholder="Email"><br />
                <label for="spol">*Spol:</label><input type="radio" name="spol" value="Z">Ženski
              	<input type="radio" name="spol" value="M">Muški<br />               
                <label for="iskustvo">Iskustvo :</label> <select name="iskustvo"><br />
                        <option value="Pacijent" selected="selected">Pacijent</option><br />
                        <option value="Nutricionist">Nutricionist</option><br />
                        <option value="Doktor">Doktor</option></select><br />
                    
               	<label for="bol">Bol:</label> <input name="bol" type="hidden" id="bol" value="ne">
               	<input name="bol" type="checkbox" id="bol" value="da"><br />
				<label for="poruka">U slučaju boli - opišite bol:</label><br /> <textarea name="poruka" id="poruka" placeholder="Poruka"></textarea><br /><br />
               	<input class="submit" type="submit" name="potvrda" value="Registracija"><br />
            </form>
            </td>
            </tr>
            </table>
</div>

<?php 

}
else{
	$ime = ($_POST['ime']);
	$prezime = ($_POST['prezime']);
	$username = ($_POST['username']);
	$password = ($_POST['password']);
	$email = ($_POST['email']);
	$spol = ($_POST['spol']);
	$iskustvo = ($_POST['iskustvo']);
	$bol = ($_POST['bol']);
	$poruka = ($_POST['poruka']);
	
	$c = new mysqli("localhost",
					"root",
					"123",
					"dwa2");
	mysqli_set_charset($c, "utf8");				
	$sql = "SELECT username, email FROM user WHERE username='$username' || email='$email' LIMIT 1";
	
	$r=$c->query($sql);
	if($r->num_rows!=0 || $username=='Nepoznat'){

		echo '<br /><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Postojeći username/email je korišten! Unesite ponovo!</p>';
		echo '<br />';
		echo '<br /><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="registracija.php">Povratak na registraciju</a></p>';
		echo '<br />';
		echo '<br /><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.html">Vrati se na naslovnu stranicu</a></p>';

	} 
	else {
		if(strlen($ime)<2 || strlen($prezime)<2 || strlen($username)<2 || strlen($password)<2 || strlen($email)<2 || !isset($spol)){

			echo '<br /><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Polja označena sa * ne smiju biti prazna ili kraća od 2 znaka! Unesite ponovo!</p>';
			echo '<br />';
			echo '<br /><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="registracija.php">Povratak na registraciju</a></p>';
			echo '<br />';
			echo '<br /><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.html">Vrati se na naslovnu stranicu</a></p>';

		}
		else{
			$password = md5($_POST['password']);
			$sql = "INSERT INTO user (ime,prezime,username,password,email,spol,iskustvo,bol,poruka)
					VALUES ('$ime','$prezime','$username','$password','$email','$spol','$iskustvo','$bol','$poruka')";		
			$c->query($sql);

			echo '<br /><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Uspješno ste registrirani! <a href="index.html">Prijava</a>';

			$c->close();
		}
	}
}

?>
    </div>	
    <div id="footer">
   	Marco Krevatin - Dinamičke Web Aplikacije 2
    </div>
</div>
</body>
</html>