<?php 
session_start();
if(!isset($_SESSION['login']))
{  
	header("Location: index.html");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clanak</title>
<link rel="stylesheet" href="css1.css" type="text/css" />
</head>

<body>
<div id="container">
    <div id="header">
        <h1><a href="profil.php">Bolnica</a></h1>
    </div>
    <div id="menu">
    <div style="width:50%; margin:0 auto;">    
    <p><a href="profil.php">Povratak na profil</a></p>
    <p><a href="korisnik.php">Izmjena podataka</a></p>
    <p><a href="odjava.php">Odjava</a></p>

<?php

if(isset($_POST['boja'])){
	$boja = $_POST['boja'];
	setcookie("boja",$boja,time()+60*60*24*30);
	header("Location: profil.php");
}
if(!isset($_COOKIE['boja'])){
	$boja = "#9AC0CD";
}
else{
	$boja = $_COOKIE['boja'];
}
?>
</div>
</div>
    <div id="content">
	<div style="width:90%; margin:0 auto;">
<?php
$c = new mysqli("localhost",
				"root",
				"123",
				"dwa2");
mysqli_set_charset($c, "utf8");
if(!isset($_GET['id_vijest'])){
	$id_vijest = '';
}else
{
	$id_vijest = $_GET['id_vijest'];
}
$sql = "SELECT * FROM vijest WHERE id_vijest='" . $id_vijest . "'";
$r = $c->query($sql);
$row = $r->fetch_assoc(); 

$id_suser = $row['id_suser'];
$sql2 = "SELECT * FROM suser WHERE id_suser='" . $id_suser . "'";
$r2 = $c->query($sql2);
$row2 = $r2->fetch_assoc();

echo '<br />
	<table width="100%" style="background-color:'. $boja .'; border-radius:5px;">
		<tr>
			<td>'. 'Napisao: '. $row2['tip'] .' ' . $row2['ime'].' '. $row2['prezime']. '</td>
			<td> </td>
			<td>'. 'Objavljeno: ' . $row['datum_vijest'] .'</td>
		</tr>
		<tr>
		<td> </td><td><h3>'.$row['naslov'] .'</h3></td><td></td>
		</tr>
		
		<tr>
			<td colspan="3"><div>'. $row['tekst'] .'</div></td>
		</tr>
		
	</table>';
?>	

<form method="post" id="form_komentar" name="komentar" action="">
<p>Unesi komentar</p>
<p><textarea name="komentar" id="komentar"></textarea></p> 
<p><input class="submit" type="submit" name="potvrda" value="Komentiraj" /></p>
</form>

<?php
mysqli_set_charset($c, "utf8");
if(!isset($_POST['komentar'])){
}else{
$komentar = $_POST['komentar'];
}
date_default_timezone_set("Europe/Zagreb");
$datum_kom = date("Y-m-d");
$vrijeme_kom = date("H:i:s");

if(!empty($komentar))
{
$sql3 = "SELECT id_user FROM user WHERE email='". $_SESSION['login'] ."'";
$r3 = $c->query($sql3);
$row3 = $r3->fetch_assoc();

$sql4 = "INSERT INTO komentar (id_user,komentar,datum_kom,vrijeme_kom) VALUES ('". $row3['id_user'] ."','$komentar','$datum_kom','$vrijeme_kom')";
$c->query($sql4);

$sql41 = "SELECT id_komentar FROM komentar WHERE datum_kom='". $datum_kom ."' AND vrijeme_kom='". $vrijeme_kom ."'";
$r4 = $c->query($sql41);
$row4 = $r4->fetch_assoc();

	$sql5 = "INSERT INTO komentarvijest(id_komentar, id_vijest) VALUES ('". $row4['id_komentar'] ."', '". $row['id_vijest'] ."')";
	$c->query($sql5);
}

$s = "SELECT * FROM komentarvijest WHERE id_vijest='$id_vijest' ORDER BY id_poveznica DESC";
$res = $c->query($s);

while($ro = $res->fetch_assoc()){
	$s2 = "SELECT * FROM komentar WHERE id_komentar='". $ro['id_komentar'] ."'";
	$res2 = $c->query($s2);
	$ro2 = $res2->fetch_assoc();
	
	$s3 = "SELECT username,poruka,iskustvo,spol FROM user WHERE id_user='". $ro2['id_user'] ."'";
	
	$res3 = $c->query($s3);
	$ro3 = $res3->fetch_assoc();
	
	if(empty($ro3)) {
		$ro3['username']="Nepoznat";	
		$ro3['iskustvo']="N";
		$ro3['spol']="N";
		$ro3['poruka']="N";		
	}
	
	echo  $ro3['iskustvo'] .' '. $ro3['username'] .' je komentirao/la
	<table max-width="400px;" width="300px;" style="background-color:'. $boja .'; border-collapse: collapse; border-radius:5px;">
		<tr align="right">
			<td><p style="font-size:12px;">'. $ro2['datum_kom'].' '.$ro2['vrijeme_kom'] .'</p></td>
		</tr>
		<tr>
			<td colspan="3">'. $ro2['komentar'] .'</td>
		</tr>
	</table>
	<br />';
}

?>     
</div></div>
<div id="footer">
        Marco Krevatin - Dinamičke Web Aplikacije 2
    </div>
</body>
</html>